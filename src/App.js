import { Route, Routes, useNavigate } from "react-router-dom";
import Layout from "./layout/Layout";
import Home from "./pages/home/Home";
import Allfiles from "./pages/allFiles/Allfiles";
import Starred from "./pages/starred/Starred";
import Archived from "./pages/archived/Archived";
function App() {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route index element={<Home />} />
        <Route path="/Allfiles" element={<Allfiles />} />
        <Route path="/Starred" element={<Starred />} />
        <Route path="/Archived" element={<Archived />} />
      </Route>
    </Routes>
  );
}

export default App;
