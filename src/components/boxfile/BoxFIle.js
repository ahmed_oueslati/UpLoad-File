import React from "react";
import "./BoxFile.css";

function BoxFIle(props) {
  return (
    <div className={props.className}>
      <i
        style={{
          backgroundColor:
            props.className === "starredFiles" ? "#f2cd5c" : "#cd0404",
        }}
      >
        {props.icon}
      </i>
      <div className="typographie">
        <h2>{props.title}</h2>
        <p>go to view</p>
      </div>
    </div>
  );
}

export default BoxFIle;
