import React from "react";
import "./CardFile.css";

function CardFile(props) {
  return (
    <div className="typefile">
      <i
        style={{
          backgroundColor:
            props.className === "documentCard"
              ? "#3870A3"
              : props.className === "imageCard"
              ? "#0081b4"
              : props.className === "vedioCard"
              ? "#1f8a70"
              : null,
        }}
      >
        {props.icon}
      </i>
      <p>{props.title}</p>
      <div className="progression">
        <div className="progressBar">
          <div
            style={{
              width: `${props.percentage}%`,
              backgroundColor:
                props.className === "documentCard"
                  ? "#362fd9"
                  : props.className === "imageCard"
                  ? "#0081b4"
                  : props.className === "vedioCard"
                  ? "#1f8a70"
                  : null,
              height: "100%",
              borderRadius: "1rem",
            }}
          ></div>
        </div>
        <div className="statistic">
          <p>
            <span>{props.percentage}%</span> {props.stockage}gb of 2048gb
          </p>
        </div>
      </div>
    </div>
  );
}

export default CardFile;
