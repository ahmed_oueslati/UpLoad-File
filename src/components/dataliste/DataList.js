import "./DataList.css";
import React from "react";
function DataList(props) {
  return (
    <div className="dataList">
      <div className="headList">
        <p>Name</p>
        <p>Cerated date</p>
        <p>Size</p>
        {window.location.pathname !== "/" && <p>Action</p>}
      </div>
      <div className="mainList">
        {props.allElements}
        {props.elementToArchive}
        {props.elementStarred}
        {props.recentElements}
      </div>
    </div>
  );
}

export default DataList;
