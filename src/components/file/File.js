import React from "react";
import "./File.css";
import ArchiveIcon from "../icons/ArchiveIcon";
import StarIcon from "../icons/StarIcon";
import DeleteIcon from "../icons/DeleteIcon";
import Arrow from "../icons/Arrow";
import { useGlobalContext } from "../../context/DataContext";
import { FcFile, FcVideoCall, FcEditImage } from "react-icons/fc";
import { BsImage } from "react-icons/bs";

function File({ name, id, size, date, type }) {
  const fulDate = new Date(date);
  const day = fulDate.getDay();
  const month = fulDate.getMonth();
  const year = fulDate.getFullYear();

  const {
    handleArchiv,
    handleStar,
    handleDelete,
    handleArrowArchiv,
    handleArrowStar,
  } = useGlobalContext();
  return (
    <div className="file">
      <div className="iconName">
        <div className="icon">
          {type?.includes("video") || name?.includes("mp4")
            ? FcVideoCall()
            : type?.includes("image") || name?.includes("svg")
            ? FcEditImage()
            : FcFile()}

          {}
        </div>
        <div className="name">
          <p>{name}</p>
        </div>
      </div>
      <div className="dateCreat">
        {day}/{month}/{year}
      </div>
      <div className="size">{size}</div>
      {window.location.pathname !== "/" && (
        <div className="actions">
          {window.location.pathname !== "/" &&
          window.location.pathname !== "/Allfiles" ? (
            window.location.pathname === "/Archived" ? (
              <>
                <div className="delete" onClick={() => handleDelete(id)}>
                  {DeleteIcon()}
                </div>
                <div className="arrow" onClick={() => handleArrowArchiv(id)}>
                  {Arrow()}
                </div>
              </>
            ) : (
              <div className="arrowstar" onClick={() => handleArrowStar(id)}>
                {StarIcon()}
              </div>
            )
          ) : (
            <>
              <div className="star" onClick={() => handleStar(id)}>
                {StarIcon()}
              </div>
              <div className="archive" onClick={() => handleArchiv(id)}>
                {ArchiveIcon()}
              </div>
            </>
          )}
        </div>
      )}
    </div>
  );
}

export default File;
