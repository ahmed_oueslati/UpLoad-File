import React from "react";
import "./HeaderMain.css";
import { AiOutlineDown } from "react-icons/ai";

function HeaderMain(props) {
  return (
    <header className="headerMainContent">
      <div className="logoMainContent">
        <i
          style={{
            backgroundColor:
              props.class === "AllFileLogo"
                ? "#3870A3"
                : props.class === "ArchivedLogo"
                ? "#cd0404"
                : props.class === "starredLogo"
                ? "#f2cd5c"
                : null,
          }}
        >
          {props.icon}
        </i>
        <h1>{props.title}</h1>
      </div>
      <div
        className="sortMainContent"
        style={{
          backgroundColor:
            props.class === "AllFileLogo"
              ? "#3870A3"
              : props.class === "ArchivedLogo"
              ? "#cd0404"
              : props.class === "starredLogo"
              ? "#f2cd5c"
              : null,
        }}
      >
        <p onClick={props.sort}>By date</p>
        <i>
          <AiOutlineDown />
        </i>
      </div>
    </header>
  );
}

export default HeaderMain;
