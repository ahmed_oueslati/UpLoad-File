import React from "react";
import "./ArchiveIcon.css";
import { BsArchive } from "react-icons/bs";

function ArchiveIcon() {
  return (
    <div className="archiveicon">
      <i>{BsArchive()}</i>
    </div>
  );
}

export default ArchiveIcon;
