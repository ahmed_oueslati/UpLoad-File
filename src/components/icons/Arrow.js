import React from "react";
import "./Arrow.css";
import { TiArrowBackOutline } from "react-icons/ti";

function Arrow() {
  return (
    <div className="arrowicon">
      <i>{TiArrowBackOutline()}</i>
    </div>
  );
}

export default Arrow;
