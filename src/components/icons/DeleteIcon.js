import React from "react";
import "./DeleteIcon.css";
import { AiOutlineDelete } from "react-icons/ai";
function DeleteIcon() {
  return (
    <div className="deleteicon">
      <i>{AiOutlineDelete()}</i>
    </div>
  );
}

export default DeleteIcon;
