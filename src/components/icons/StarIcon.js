import React from "react";
import "./StarIcon.css";
import { AiOutlineStar, AiFillStar } from "react-icons/ai";
import { useGlobalContext } from "../../context/DataContext";
function StarIcon() {
  const { isStarred } = useGlobalContext();
  return (
    <div className="staricon">
      <i>
        {window.location.pathname !== "/Starred"
          ? AiOutlineStar()
          : AiFillStar()}
      </i>
    </div>
  );
}

export default StarIcon;
