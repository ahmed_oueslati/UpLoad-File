import React from "react";
import "./SideBarmodal.css";
import Sidebar from "../../layout/sidebar/Sidebar";

function SideBarModal() {
  return (
    <div className="modalContent">
      <aside className="sidFit">
        <Sidebar />
      </aside>
    </div>
  );
}

export default SideBarModal;
