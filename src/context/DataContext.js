import React, { createContext } from "react";
import File from "../components/file/File";
import { nanoid } from "nanoid";

const DataContext = createContext({});

export const DataProvider = ({ children }) => {
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const [files, setFiles] = React.useState([]);
  const [starred, setStarred] = React.useState([]);
  const [archived, setArchived] = React.useState([]);
  const [documents, setDocuments] = React.useState(0);
  const [images, setImages] = React.useState(0);
  const [videos, setVideos] = React.useState(0);

  function formatFileSize(size) {
    const units = ["bytes", "kilobytes", "megabytes", "gigabytes"];
    let i = 0;

    while (size >= 1024 && i < units.length - 1) {
      size /= 1024;
      i++;
    }
    return size.toFixed(2);
  }

  // handleUpload function to upload files
  function handleUpload(event) {
    const file = event.target.files[0];
    if (file?.type?.includes("video")) {
      const videoSize = +formatFileSize(file.size);
      if (videos + videoSize <= 2048) {
        setVideos((prev) => (prev += videoSize));
        setFiles([...files, file]);
      }
    } else if (file?.type?.includes("image")) {
      const imageSize = +formatFileSize(file.size);
      if (images + imageSize <= 2048) {
        setImages((prev) => (prev += imageSize));
        setFiles([...files, file]);
      }
    } else {
      const documentSize = +formatFileSize(file.size);
      if (documents + documentSize <= 2048) {
        setDocuments((prev) => (prev += documentSize));
        setFiles([...files, file]);
      }
    }
  }
  //

  files.map((file) => console.log(file));
  console.log(files);

  // render all file
  const elementsList = files.map((file) => (
    <File
      key={nanoid()}
      name={file.name}
      id={file.lastModified}
      date={file.lastModified}
      size={file.size}
      type={file.type}
    />
  ));
  //

  // put element to archiv
  function handleArchiv(id) {
    const elementToArchive = files.filter((item) => item.lastModified === id);
    setArchived([...archived, elementToArchive]);
    setFiles((prevFiles) => {
      return prevFiles.filter((file) => file.lastModified !== id);
    });
  }
  //

  // delete element
  function handleDelete(id) {
    console.log("delet");
    console.log(id);
    console.log(archived[0]);
    setArchived((prevArchived) => {
      return prevArchived.filter((file) => file[0].lastModified !== id);
    });
  }
  //

  // arrow element archived
  function handleArrowArchiv(id) {
    console.log(files);
    const element = archived.filter((file) => file[0].lastModified === id);
    console.log(element[0]);
    setFiles((prevFiles) => [...prevFiles, element[0][0]]);
    setArchived((prevArchived) => {
      return prevArchived.filter((file) => file[0].lastModified !== id);
    });
  }
  //
  //arrow element stared
  function handleArrowStar(id) {
    console.log(files);
    const element = starred.filter((file) => file[0].lastModified === id);
    console.log(element[0]);
    setFiles((prevFiles) => [...prevFiles, element[0][0]]);
    setStarred((prevArchived) => {
      return prevArchived.filter((file) => file[0].lastModified !== id);
    });
    //setIsStarred((prev) => (prev = !prev));
  }
  //
  // render element archived
  const elementArchivedList = archived.map((item, index) => (
    <File
      key={nanoid()}
      id={item[0].lastModified}
      date={item[0].lastModified}
      name={item[0].name}
      type={item[0].type}
      size={item[0].size}
    />
  ));
  //

  // put element to starred
  function handleStar(id) {
    const arrayStarred = starred.filter((item) => item[0].lastModified === id);
    if (arrayStarred.length === 0) {
      const elementToStared = files.filter((item) => item.lastModified === id);
      setStarred([...starred, elementToStared]);
      setFiles((prevFiles) => {
        return prevFiles.filter((file) => file.lastModified !== id);
      });
      //setIsStarred((prev) => (prev = !prev));
    } else {
      setStarred((prevStared) =>
        prevStared.filter((item) => item[0].lastModified !== id)
      );
    }
  }
  //

  // render elements starred
  //console.log("render 1", starred);
  const elementsStarredList = starred.map((item) => (
    <File
      key={nanoid()}
      id={item[0]?.lastModified}
      date={item[0]?.lastModified}
      name={item[0]?.name}
      type={item[0]?.type}
      size={item[0]?.size}
      del={handleDelete}
    />
  ));
  //

  // sort files
  function handleSortAllFiles() {
    console.log("all sort");
    const allFilesSorted = files
      ?.map((obj) => {
        return {
          name: obj?.name,
          size: obj?.size,
          lastModified: new Date(obj?.lastModified),
        };
      })
      .sort((a, b) => a?.lastModified - b?.lastModified);
    setFiles(allFilesSorted);
  }

  function handleSortStarredFiles() {
    const star = starred.flat();
    const starredFilesSorted = star
      ?.map((obj) => {
        return {
          name: obj.name,
          size: obj.size,
          lastModified: new Date(obj.lastModified),
        };
      })
      .sort((a, b) => a?.lastModified - b?.lastModified);
    const newStarredArray = starredFilesSorted.map((item) => [item]);
    setStarred(newStarredArray);
  }

  function handleSortArchivedFiles() {
    const archiv = archived.flat();
    const archivFilesSorted = archiv
      ?.map((obj) => {
        return {
          name: obj.name,
          size: obj.size,
          lastModified: new Date(obj.lastModified),
        };
      })
      .sort((a, b) => a?.lastModified - b?.lastModified);
    const newArchivedArray = archivFilesSorted.map((item) => [item]);
    setArchived(newArchivedArray);
  }

  const initialState = {
    handleSortAllFiles,
    handleSortArchivedFiles,
    handleSortStarredFiles,
    handleUpload,
    files,
    setFiles,
    elementsList,
    elementArchivedList,
    elementsStarredList,
    handleArrowArchiv,
    handleDelete,
    isModalOpen,
    setIsModalOpen,
    handleArchiv,
    handleStar,
    starred,
    setStarred,
    archived,
    setArchived,
    documents,
    images,
    videos,
    formatFileSize,
    handleArrowStar,
  };
  return (
    <DataContext.Provider value={initialState}>{children}</DataContext.Provider>
  );
};

export const useGlobalContext = () => {
  return React.useContext(DataContext);
};
