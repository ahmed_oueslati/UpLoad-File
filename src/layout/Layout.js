import React from "react";
import "./Layout.css";
import Header from "./header/Header";
import Sidebar from "./sidebar/Sidebar";
import SideBarModal from "../components/modal/SideBarModal";
import { Outlet } from "react-router-dom";
import { useGlobalContext } from "../context/DataContext";

function Layout() {
  const { isModalOpen, setIsModalOpen } = useGlobalContext();
  return (
    <div className="layout">
      <aside className="sidLarge">
        <Sidebar />
      </aside>

      <div className="mainLayout">
        <Header />
        <div className="mainLayoutContent">
          <Outlet />
        </div>
      </div>
      {isModalOpen && <SideBarModal />}
    </div>
  );
}

export default Layout;
