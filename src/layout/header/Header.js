import React from "react";
import "./Header.css";
import { FiSearch } from "react-icons/fi";
import { AiOutlineSetting, AiOutlineMenu } from "react-icons/ai";
import { IoIosNotificationsOutline } from "react-icons/io";
import { FaUser } from "react-icons/fa";
import { useGlobalContext } from "../../context/DataContext";

export default function Header() {
  const { isModalOpen, setIsModalOpen } = useGlobalContext();
  return (
    <>
      <header className="topHeader">
        <div
          className="menu"
          onClick={() => setIsModalOpen((prev) => (prev = true))}
        >
          <i>{AiOutlineMenu()}</i>
        </div>
        <div className="headerSearch">
          <div>{FiSearch()}</div>
          <input type="text" placeholder="Search.."></input>
        </div>
        <div className="header--user">
          <div className="setting">{AiOutlineSetting()}</div>
          <div className="notification">
            {IoIosNotificationsOutline()}
            <p className="notiNumber">17</p>
          </div>
          <div className="user--info">
            <p className="user--name">Ahmed</p>
            <div className="user--img">{FaUser()}</div>
          </div>
        </div>
      </header>
    </>
  );
}
