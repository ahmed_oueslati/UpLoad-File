import React from "react";
import "./Sidebar.css";
import { FiUploadCloud } from "react-icons/fi";
import { BsArchive } from "react-icons/bs";
import { SiUploaded } from "react-icons/si";
import {
  AiOutlineHome,
  AiOutlineFileSearch,
  AiOutlineStar,
} from "react-icons/ai";
import { NavLink } from "react-router-dom";
import { useGlobalContext } from "../../context/DataContext";

export default function Sidebar() {
  const { handleUpload, isModalOpen, setIsModalOpen } = useGlobalContext();

  return (
    <>
      <div
        className="logo"
        onClick={() => setIsModalOpen((prev) => (prev = false))}
      >
        <SiUploaded />
      </div>
      <div className="sideContent">
        <form>
          <label htmlFor="uploder">
            <span>
              <FiUploadCloud />
            </span>
            <p>Upload</p>
          </label>
          <input
            id="uploder"
            type="file"
            style={{ display: "none" }}
            onChange={handleUpload}
          />
        </form>
        <main className="sideMain">
          <ul>
            <li onClick={() => setIsModalOpen((prev) => (prev = false))}>
              <NavLink to="/">
                <i>
                  <AiOutlineHome />
                </i>
                <p>Home</p>
              </NavLink>
            </li>
            <li onClick={() => setIsModalOpen((prev) => (prev = false))}>
              <NavLink
                to="/Allfiles"
                style={({ isActive }) => {
                  return isActive
                    ? {
                        backgroundColor: "#3870A3",
                      }
                    : {};
                }}
              >
                <i>
                  <AiOutlineFileSearch />
                </i>
                <p>All files</p>
              </NavLink>
            </li>
            <li onClick={() => setIsModalOpen((prev) => (prev = false))}>
              <NavLink
                to="/Starred"
                style={({ isActive }) => {
                  return isActive
                    ? {
                        backgroundColor: "rgb(242, 205, 92)",
                      }
                    : {};
                }}
              >
                <i>
                  <AiOutlineStar />
                </i>
                <p>Starred</p>
              </NavLink>
            </li>
            <li onClick={() => setIsModalOpen((prev) => (prev = false))}>
              <NavLink
                to="/Archived"
                style={({ isActive }) => {
                  return isActive
                    ? {
                        backgroundColor: "rgb(205, 4, 4)",
                      }
                    : {};
                }}
              >
                <i>
                  <BsArchive />
                </i>
                <p>Archived</p>
              </NavLink>
            </li>
          </ul>
        </main>
      </div>
    </>
  );
}
