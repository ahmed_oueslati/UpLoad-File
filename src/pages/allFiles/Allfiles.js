import React from "react";
import "./Allfiles.css";
import HeaderMain from "../../components/headermain/HeaderMain";
import DataList from "../../components/dataliste/DataList";
import { AiOutlineFileSearch } from "react-icons/ai";
import { useGlobalContext } from "../../context/DataContext";

function Allfiles() {
  const { elementsList, handleSortAllFiles } = useGlobalContext();

  return (
    <section className="allFileSection">
      <HeaderMain
        icon={<AiOutlineFileSearch />}
        class={"AllFileLogo"}
        title={"All files"}
        sort={handleSortAllFiles}
      />
      <DataList allElements={elementsList} />
    </section>
  );
}

export default Allfiles;
