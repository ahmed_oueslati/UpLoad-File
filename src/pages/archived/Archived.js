import React from "react";
import "./Archived.css";
import HeaderMain from "../../components/headermain/HeaderMain";
import DataList from "../../components/dataliste/DataList";
import { BsArchive } from "react-icons/bs";
import { useGlobalContext } from "../../context/DataContext";

function Archived() {
  const { elementArchivedList, handleSortArchivedFiles } = useGlobalContext();

  return (
    <section className="ArchivSection">
      <HeaderMain
        icon={<BsArchive />}
        class={"ArchivedLogo"}
        title={"Archived files"}
        sort={handleSortArchivedFiles}
      />
      <DataList elementToArchive={elementArchivedList} />
    </section>
  );
}

export default Archived;
