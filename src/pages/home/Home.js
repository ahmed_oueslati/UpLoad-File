import React from "react";
import "./Home.css";
import BoxFIle from "../../components/boxfile/BoxFIle";
import CardFile from "../../components/cardFile/CardFile";
import DataList from "../../components/dataliste/DataList";
import File from "../../components/file/File";
import {
  AiOutlineFile,
  AiOutlineVideoCameraAdd,
  AiOutlineStar,
} from "react-icons/ai";
import { BsCardImage } from "react-icons/bs";
import { useGlobalContext } from "../../context/DataContext";
import { BsArchive } from "react-icons/bs";
import { NavLink } from "react-router-dom";

function Home() {
  const { starred, archived, files, documents, images, videos } =
    useGlobalContext();

  const starNum = starred.length;
  const archivNum = archived.length;

  const recentElementsList = files.map((file, index) => (
    <File
      key={index}
      name={file.name}
      id={file.lastModified}
      date={file.lastModified}
      size={file.size}
      type={file.type}
    />
  ));

  return (
    <section className="homeSection">
      <div className="sectionTop">
        <CardFile
          icon={<AiOutlineFile />}
          title={"Documents"}
          className={"documentCard"}
          progresse={"documentProgresse"}
          percentage={((documents * 100) / 2048).toFixed(2)}
          stockage={documents}
        />
        <CardFile
          icon={<BsCardImage />}
          title={"Images"}
          className={"imageCard"}
          progresse={"imageProgresse"}
          percentage={((images * 100) / 2048).toFixed(2)}
          stockage={images}
        />
        <CardFile
          icon={<AiOutlineVideoCameraAdd />}
          title={"videos"}
          className={"vedioCard"}
          progresse={"vedioProgresse"}
          percentage={((videos * 100) / 2048).toFixed(2)}
          stockage={videos}
        />
      </div>
      <div className="sectionBottom">
        <div className="recentFiles">
          <h1>Recent Files</h1>
          <DataList recentElements={recentElementsList} />
        </div>
        <div className="filesSelected">
          <NavLink to="/Starred">
            <BoxFIle
              className={"starredFiles"}
              icon={<AiOutlineStar />}
              title={`${starNum} Starred Files`}
            />
          </NavLink>
          <NavLink to="/Archived">
            <BoxFIle
              className={"archivedFiles"}
              icon={<BsArchive />}
              title={`${archivNum} Archived Files`}
            />
          </NavLink>
        </div>
      </div>
    </section>
  );
}

export default Home;
