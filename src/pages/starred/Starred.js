import React from "react";
import "./Starred.css";
import HeaderMain from "../../components/headermain/HeaderMain";
import DataList from "../../components/dataliste/DataList";
import { AiOutlineStar } from "react-icons/ai";
import { useGlobalContext } from "../../context/DataContext";
function Starred() {
  const { elementsStarredList, handleSortStarredFiles } = useGlobalContext();

  return (
    <section className="staredSection">
      <HeaderMain
        icon={<AiOutlineStar />}
        class={"starredLogo"}
        title={"Starred files"}
        sort={handleSortStarredFiles}
      />
      <DataList elementStarred={elementsStarredList} />
    </section>
  );
}

export default Starred;
